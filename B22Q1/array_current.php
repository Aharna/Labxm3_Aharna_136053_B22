<?php
/*The current() function returns the value of the current element in an array. Every array has an internal pointer to its "current" element, which is initialized to the first element inserted into the array. This function does not move the arrays internal pointer. */
$a=array("pen","pencil","erager");
print_r(current($a));

?>