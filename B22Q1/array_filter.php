<?php
/*The array_filter() function filters the values of an array using a callback function.
This function passes each value of the input array to the callback function. If the callback function returns true, the current value from input is returned into the result array. Array keys are preserved.
*/
function odd($a){
    return ($a & 1);
}
function even($a){
    return (!($a & 1));
}
$array1=array("a"=>1,"b"=>2,"c"=>4,"d"=>3);
$array2=array(1,2,3,4,5);
$c= array_filter($array1,"even");
$d=array_filter($array2,"odd");
echo "<pre>";
print_r ($c);
print_r ($d);
?>