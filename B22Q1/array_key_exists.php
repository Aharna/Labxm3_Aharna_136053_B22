<?php
/*The array_key_exists() function checks an array for a specified key, and returns true if the key exists and false if the key does not exist.
Remember that if you skip the key when you specify an array, an integer key is generated, starting at 0 and increases by 1 for each value. (See example 2)
*/
$a=array("pen"=>13,"pencil"=>15,"erager"=>20);
if(array_key_exists("pen",$a))
{
    echo "There is a key";
}
else
{
    echo "There is no key";
}
?>