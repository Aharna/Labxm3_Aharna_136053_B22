<?php
/*Clean code is subjective and every developer has a personal take on it. There are some ideas that are considered best practice and what constitutes as clean code within the industry and community, but there is no definitive distinction. And I don’t think there ever will be.
*necessary of clean code
 *
    It is easy to understand the execution flow of the entire application
    It is easy to understand how the different objects collaborate with each other
    It is easy to understand the role and responsibility of each class
    It is easy to understand what each method does
    It is easy to understand what is the purpose of each expression and variable

 */
?>